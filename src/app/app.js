angular.module('frontend', [
  'ngRoute',
  'ngResource',
  'ngProgress',
  'charts',
  'device',
  'frontend.todo'
])
  .constant('HOST_DOMAIN', 'http://localhost:8080') //http://localhost:8080
  .constant('METRIC_DATA', [
    {'IP_AD':'127.0.0.1', 'DATE_AND_TIME':'2016-06-24 10:00', 'CPU_USAGE':'09', 'MEMORY':'1100', 'NETWORK_IN':'21000', 'NETWORK_OUT':'32000'},
    {'IP_AD':'127.0.0.1', 'DATE_AND_TIME':'2016-06-24 12:00', 'CPU_USAGE':'09', 'MEMORY':'1150', 'NETWORK_IN':'21500', 'NETWORK_OUT':'32500'},
    {'IP_AD':'127.0.0.1', 'DATE_AND_TIME':'2016-06-25 10:00', 'CPU_USAGE':'12', 'MEMORY':'1250', 'NETWORK_IN':'22500', 'NETWORK_OUT':'33500'},
    {'IP_AD':'127.0.0.1', 'DATE_AND_TIME':'2016-06-25 12:00', 'CPU_USAGE':'12', 'MEMORY':'1350', 'NETWORK_IN':'23500', 'NETWORK_OUT':'34500'},
    {'IP_AD':'127.0.0.1', 'DATE_AND_TIME':'2016-06-26 10:00', 'CPU_USAGE':'10', 'MEMORY':'1200', 'NETWORK_IN':'22000', 'NETWORK_OUT':'33000'},
    {'IP_AD':'127.0.0.1', 'DATE_AND_TIME':'2016-06-26 12:00', 'CPU_USAGE':'11', 'MEMORY':'1300', 'NETWORK_IN':'23000', 'NETWORK_OUT':'34000'},
    {'IP_AD':'127.0.0.2', 'DATE_AND_TIME':'2016-06-26 12:00', 'CPU_USAGE':'12', 'MEMORY':'1400', 'NETWORK_IN':'24000', 'NETWORK_OUT':'35000'},
    {'IP_AD':'127.0.0.2', 'DATE_AND_TIME':'2016-06-26 10:00', 'CPU_USAGE':'13', 'MEMORY':'1500', 'NETWORK_IN':'25000', 'NETWORK_OUT':'36000'}])
.run(function($rootScope, ngProgressFactory) {
  $rootScope.progressbar = ngProgressFactory.createInstance();
  $rootScope.$on('$routeChangeStart', function(ev,data) {
    $rootScope.progressbar.start();
  });
  $rootScope.$on('$routeChangeSuccess', function(ev,data) {
    $rootScope.progressbar.complete();
  });
})
.config(function ($routeProvider) {
  'use strict';

  $routeProvider
    .when('/todo', {
      controller: 'TodoCtrl',
      templateUrl: 'todo/todo.html'
    })
    .when('/amcharts', {
      controller: 'AmchartCtrl',
      templateUrl: 'amcharts/amchart.html',
      resolve: {
        devicesList: function(devicesListFactory) {
          return devicesListFactory.getDevicesList();
        }
      }
    })
    .when('/devices', {
      controller: 'DeviceCtrl',
      templateUrl: 'devices/devices.html',
      resolve: {
        metricsList: function(metricsListFactory) {
          console.log(metricsListFactory.getMetricsList());
          return metricsListFactory.getMetricsList();
        }
      }
    })
    .otherwise({
      redirectTo: '/todo'
    });
});
